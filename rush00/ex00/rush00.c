/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wfournie <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/03 12:06:44 by wfournie          #+#    #+#             */
/*   Updated: 2019/08/04 16:47:20 by wfournie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	select_print(int colonne_max, int ligne_max, int colonne, int ligne)
{
	if ((ligne == 0 && colonne == 0)
			|| (ligne == 0 && colonne == colonne_max)
			|| (ligne == ligne_max && colonne == 0)
			|| (ligne == ligne_max && colonne == colonne_max))
	{
		ft_putchar('o');
	}
	else if ((ligne == 0 && colonne != 0)
			|| (ligne == 0 && colonne != colonne_max)
			|| (ligne == ligne_max && colonne != 0)
			|| (ligne == ligne_max && colonne != colonne_max))
	{
		ft_putchar('-');
	}
	else if (ligne != 0
			&& ligne != ligne_max
			&& colonne != 0
			&& colonne != colonne_max)
	{
		ft_putchar(' ');
	}
	else
	{
		ft_putchar('|');
	}
}

void	rush(int colonne_max, int ligne_max)
{
	int		colonne;
	int		ligne;

	if (ligne_max > 0 && colonne_max > 0)
	{
		ligne_max = ligne_max - 1;
		colonne_max = colonne_max - 1;
		ligne = 0;
		while (ligne <= ligne_max)
		{
			colonne = 0;
			while (colonne <= colonne_max)
			{
				select_print(colonne_max, ligne_max, colonne, ligne);
				colonne++;
			}
			ft_putchar('\n');
			ligne++;
		}
	}
}
