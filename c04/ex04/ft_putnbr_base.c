/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/05 09:29:52 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/06 23:57:28 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strlen(char *c)
{
	int i;

	i = 0;
	while (c[i] != '\0')
		i++;
	return (i);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		base_is_possible(char *base)
{
	int i;
	int j;

	j = 0;
	i = 0;
	if (ft_strlen(base) == 0 || ft_strlen(base) == 1)
		return (0);
	while (base[i] != '\0')
	{
		while (base[j] != '\0')
		{
			if ((base[i] == base[j] && i != j) ||
				(base[j] == '+' || base[j] == '-' || base[j] == '\f' ||
				base[j] == '\t' || base[j] == ' ' || base[j] == '\n' ||
				base[j] == '\r' || base[j] == '\v'))
				return (0);
			j++;
		}
		j = 0;
		i++;
	}
	return (1);
}

void	ft_putnbr_base(int nbr, char *base)
{
	unsigned int i;
	unsigned int nb;

	if (base_is_possible(base) == 1)
	{
		i = ft_strlen(base);
		if (nbr < 0)
		{
			ft_putchar('-');
			nb = nbr * (-1);
		}
		else
			nb = nbr;
		if (nb >= i)
		{
			ft_putnbr_base(nb / i, base);
			ft_putchar(base[nb % i]);
		}
		else
			ft_putchar(base[nb]);
	}
}
