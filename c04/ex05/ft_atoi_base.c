/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/06 07:53:43 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/07 04:52:13 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_strlen(char *c)
{
	int i;

	i = 0;
	while (c[i])
		i++;
	return (i);
}

int		is_in_base(char c, char *base)
{
	int i;

	i = 0;
	while (base[i] != '\0')
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int		base_is_possible(char *base)
{
	int i;
	int j;

	j = 0;
	i = 0;
	if (ft_strlen(base) == 0 || ft_strlen(base) == 1)
		return (0);
	while (base[i] != '\0')
	{
		while (base[j] != '\0')
		{
			if ((base[i] == base[j] && i != j) ||
					(base[j] == '+' || base[j] == '-' || base[j] == '\f' ||
					base[j] == '\t' || base[j] == ' ' || base[j] == '\n' ||
					base[j] == '\r' || base[j] == '\v'))
				return (0);
			j++;
		}
		j = 0;
		i++;
	}
	return (1);
}

int		ft_atoi_base(char *str, char *base)
{
	int i;
	int signe;
	int res;

	res = 0;
	signe = 1;
	i = 0;
	if (base_is_possible(base) == 0)
		return (0);
	while (str[i] == '\f' || str[i] == '\t' || str[i] == ' '
			|| str[i] == '\n' || str[i] == '\r' || str[i] == '\v')
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			signe = signe * (-1);
		i++;
	}
	while (str[i] != '\0' && is_in_base(str[i], base) != (-1))
	{
		res = (res * ft_strlen(base)) + is_in_base(str[i], base);
		i++;
	}
	return (res * signe);
}
