/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tabnum2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 20:20:18 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 20:45:45 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "checkdict.h"

void	ft_tab_to_20(char (*tabnum)[32][12])
{
	int i;
	int j;

	i = 0;
	while (i < 10)
	{
		tabnum[0][i][0] = i + '0';
		tabnum[0][i][1] = 0;
		i++;
	}
	j = 0;
	while (i < 20)
	{
		tabnum[0][i][0] = '1';
		tabnum[0][i][1] = j + '0';
		tabnum[0][i][2] = 0;
		j++;
		i++;
	}
}

void	ft_tab_to_thousand(char (*tabnum)[32][12])
{
	int i;
	int j;

	i = 20;
	j = 2;
	while (i < 28)
	{
		tabnum[0][i][0] = j + '0';
		tabnum[0][i][1] = '0';
		tabnum[0][i][2] = 0;
		j++;
		i++;
	}
	i = 0;
	tabnum[0][28][0] = '1';
	while (++i < 3)
		tabnum[0][28][i] = '0';
	tabnum[0][28][i] = 0;
	i = 0;
	tabnum[0][29][0] = '1';
	while (++i < 4)
		tabnum[0][29][i] = '0';
	tabnum[0][29][i] = 0;
}

void	ft_tab_to_billion(char (*tabnum)[32][12])
{
	int i;

	i = 0;
	tabnum[0][30][0] = '1';
	while (++i < 7)
		tabnum[0][30][i] = '0';
	tabnum[0][30][i] = 0;
	i = 0;
	tabnum[0][31][0] = '1';
	while (++i < 10)
		tabnum[0][31][i] = '0';
	tabnum[0][31][i] = 0;
}
