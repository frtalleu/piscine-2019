/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_nu.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 19:03:46 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/18 18:15:52 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"
#include <stdlib.h>
#include <unistd.h>
#include "write_nu.h"

void	ft_write_tenth_unit(struct s_read_num *nu, int nbr)
{
	int i;

	i = 0;
	if ((nbr % 100) < 20 && (nbr % 100) != 0)
	{
		while (nu[i].nb != (nbr % 100))
			i++;
		ft_putstr(nu[i].st);
		i = 0;
	}
	else if (nbr % 100 != 0)
	{
		while (nu[i].nb != ((nbr % 100) / 10) * 10)
			i++;
		ft_putstr(nu[i].st);
		if (nbr % 10 != 0)
			write(1, " ", 1);
		i = 0;
		while (nu[i].nb != (nbr % 10) && (nbr % 10) != 0)
			i++;
		if (nbr % 10 != 0)
			ft_putstr(nu[i].st);
	}
}

void	ft_write_nu(struct s_read_num *nu, long long unsigned int nbr)
{
	if (nbr / 100 > 0)
		ft_write_hundred(nu, nbr);
	ft_write_tenth_unit(nu, nbr);
}

void	ft_write_big_nu(struct s_read_num *nu, long long unsigned int nb)
{
	long long unsigned int puis;
	long long unsigned int j;

	puis = 1;
	j = 0;
	while (puis < nb)
	{
		puis = puis * 1000;
	}
	puis = puis / 1000;
	while (nu[j].nb != puis)
	{
		j++;
	}
	if (nb > 1000)
	{
		ft_write_nu(nu, nb / puis);
		write(1, " ", 1);
		ft_putstr(nu[j].st);
		if (nb % puis != 0)
			write(1, " ", 1);
		ft_write_big_nu(nu, nb % puis);
	}
	else
		ft_write_nu(nu, nb);
}

void	ft_check_in_base(struct s_read_num *nu, long long unsigned int nbr)
{
	long long unsigned int i;
	long long unsigned int puis;

	puis = 1;
	i = 0;
	while (nbr > puis)
		puis = puis * 1000;
	while (nu[i].nb != nbr && nu[i].st != NULL)
		i++;
	if (nu[i].nb == nbr)
	{
		if ((nbr == puis || nbr == 100) && nbr != 1)
		{
			ft_write_big_nu(nu, 1);
			write(1, " ", 1);
		}
		ft_putstr(nu[i].st);
	}
	else
		ft_write_big_nu(nu, nbr);
}

void	ft_check_write_nu(struct s_read_num *nu, char *str)
{
	long long unsigned int puis;
	long long unsigned int j;
	long long unsigned int nb;

	nb = ft_atoii(str);
	puis = 1;
	j = 0;
	if (ft_str_is_numeric(str) == 0)
		write(1, "Error", 5);
	if (ft_str_is_numeric(str) == 0)
		return ;
	while (puis <= nb)
		puis = puis * 1000;
	puis = puis / 1000;
	while (nu[j].nb != puis)
	{
		if (nu[j].st == NULL)
			write(1, "Error", 5);
		if (nu[j].st == NULL)
			return ;
		j++;
	}
	ft_check_in_base(nu, nb);
}
