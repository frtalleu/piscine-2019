/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 09:35:24 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 21:11:11 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <struct.h>
#include "parser.h"
#include "struct.h"

int					ft_dictlines(char *dict)
{
	int		fd;
	int		lines;
	int		distract;
	char	buf[2];

	buf[1] = 0;
	lines = 0;
	fd = open(dict, O_RDONLY);
	while (read(fd, buf, 1))
	{
		while (buf[0] == '\n' && read(fd, buf, 1))
			distract = 0;
		if (buf[0] != '\n')
			lines++;
		while (buf[0] != '\n' && read(fd, buf, 1))
			distract = 0;
	}
	close(fd);
	return (lines);
}

void				ft_nbfill(int i, int *fd, char (*buf)[2],
		t_strnum ***parsdic)
{
	char	tmp[30];
	int		j;

	j = 0;
	while (buf[0][0] >= '0' && buf[0][0] <= '9')
	{
		tmp[j++] = buf[0][0];
		read(*fd, buf[0], 1);
	}
	tmp[j] = 0;
	parsdic[0][0][i].nb = ft_atoi(tmp);
}

void				ft_pass(int *fd, char (*buf)[2])
{
	while (buf[0][0] == ' ')
		read(*fd, buf[0], 1);
	if (buf[0][0] == ':')
		read(*fd, buf[0], 1);
	while (buf[0][0] == ' ')
		read(*fd, buf[0], 1);
}

void				ft_fill(t_strnum **parsdic, char *dict)
{
	int			fd;
	static int	i;
	int			j;
	char		buf[2];

	buf[1] = 0;
	i = 0;
	fd = open(dict, O_RDONLY);
	while (read(fd, buf, 1) > 0)
	{
		j = 0;
		while (buf[0] == '\n')
			if (read(fd, buf, 1) == 0)
				return ;
		ft_nbfill(i, &fd, &buf, &parsdic);
		ft_pass(&fd, &buf);
		parsdic[0][i].st = (char *)malloc(sizeof(char) * 100);
		while (buf[0] > 31 && buf[0] < 127)
		{
			parsdic[0][i].st[j++] = buf[0];
			read(fd, buf, 1);
		}
		i++;
	}
	close(fd);
}

t_strnum			*ft_parser(char *dict)
{
	t_strnum	*parsdic;
	int			lines;

	lines = ft_dictlines(dict);
	if (!(parsdic = (t_strnum *)malloc(sizeof(t_strnum) * (lines + 1))))
		return (NULL);
	ft_fill(&parsdic, dict);
	parsdic[lines].st = NULL;
	return (parsdic);
}
