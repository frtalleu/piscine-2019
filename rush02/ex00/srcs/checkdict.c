/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tabnumb.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 09:28:43 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 20:31:11 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "checkdict.h"

int		ft_seqverif(int *fd, char (*buf)[2])
{
	int		distract;

	while (buf[0][0] == ' ')
		read(*fd, buf[0], 1);
	if (buf[0][0] != ':')
		return (0);
	while (read(*fd, buf[0], 1) && buf[0][0] == ' ')
		distract = 3;
	if (!(buf[0][0] > 31 && buf[0][0] < 127))
		return (0);
	while (buf[0][0] > 31 && buf[0][0] < 127)
		read(*fd, buf[0], 1);
	if (buf[0][0] != '\n')
		return (0);
	return (1);
}

int		ft_checklines(char *dict)
{
	int		fd;
	int		distract;
	char	buf[2];

	buf[1] = 0;
	fd = open(dict, O_RDONLY);
	if (fd < 0)
		return (0);
	while (read(fd, buf, 1) > 0)
	{
		if (buf[0] == '\n')
		{
			while (read(fd, buf, 1) > 0 && buf[0] == '\n')
				distract = 1;
			while (buf[0] >= '0' && buf[0] <= '9' && read(fd, buf, 1) > 0)
				distract = 1;
			if (ft_seqverif(&fd, &buf) < 1 && read(fd, buf, 1) > 0)
			{
				close(fd);
				return (0);
			}
		}
	}
	close(fd);
	return (1);
}

int		ft_newval(int *fd, int *i, char *dict)
{
	*i = *i + 1;
	close(*fd);
	*fd = open(dict, O_RDONLY);
	return (1);
}

int		ft_checkval(char (*tabnum)[32][12], int fd, char *dict)
{
	int		j;
	int		i;
	char	buf[2];
	char	res;

	i = 0;
	res = 0;
	buf[1] = 0;
	while (read(fd, buf, 1) > 0)
	{
		j = 0;
		while (tabnum[0][i][j] == buf[0] && read(fd, buf, 1) > 0
				&& (res == '\n' || res == 0))
		{
			if (tabnum[0][i][j + 1] == 0 && ft_seqverif(&fd, &buf))
				if (ft_newval(&fd, &i, dict))
					break ;
			j++;
		}
		res = buf[0];
		if (i == 32 && !close(fd))
			return (1);
	}
	close(fd);
	return (0);
}

int		ft_checkdict(char *dict)
{
	char	tabnum[32][12];
	int		fd;
	int		i;

	i = 0;
	ft_tab_to_20(&tabnum);
	ft_tab_to_thousand(&tabnum);
	ft_tab_to_billion(&tabnum);
	if (ft_checklines(dict) == 0)
		return (0);
	fd = open(dict, O_RDONLY);
	if (ft_checkval(&tabnum, fd, dict))
		return (1);
	return (0);
}
