/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 14:46:19 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 21:12:02 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "parser.h"
#include "checkdict.h"
#include "struct.h"
#include "write_nu.h"

void	ft_free_stuff(struct s_read_num **parsdic)
{
	int i;

	i = 0;
	while (parsdic[0][i].st != NULL)
	{
		free(parsdic[0][i].st);
		i++;
	}
	free(parsdic[0]);
}

int		ft_new_strlen(char *str)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (str[j] == '0')
		j++;
	while (str[j + i] != '\0')
		i++;
	return (i);
}

int		main(int argc, char **argv)
{
	char				*defdic;
	struct s_read_num	*parsdic;

	defdic = "srcs/numbers.dict";
	if (argc == 2 && ft_new_strlen(argv[1]) < 20)
	{
		parsdic = ft_parser(defdic);
		ft_check_write_nu(parsdic, argv[1]);
		write(1, "\n", 1);
		ft_free_stuff(&parsdic);
		return (0);
	}
	if (argc == 3 && ft_new_strlen(argv[2]) < 20)
	{
		if (!ft_checkdict(argv[1]))
			if (write(1, "Dict Error\n", 11))
				return (-1);
		parsdic = ft_parser(argv[1]);
		ft_check_write_nu(parsdic, argv[2]);
		write(1, "\n", 1);
		ft_free_stuff(&parsdic);
		return (0);
	}
	write(1, "Error\n", 6);
	return (-1);
}
