/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_nu2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 14:23:02 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 18:15:40 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "struct.h"
#include <stdlib.h>
#include <unistd.h>
#include "write_nu.h"

void						ft_putchar(char c)
{
	write(1, &c, 1);
}

void						ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

long long unsigned int		ft_atoii(char *str)
{
	int						i;
	long long unsigned int	res;

	i = 0;
	res = 0;
	while (str[i] >= '0' && str[i] <= '9')
	{
		res = (res * 10) + str[i] - 48;
		i++;
	}
	return (res);
}

int							ft_str_is_numeric(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] < 48 || str[i] > 57)
			return (0);
		i++;
	}
	return (1);
}

void						ft_write_hundred
	(struct s_read_num *nu, long long unsigned int nbr)
{
	int i;

	i = 0;
	while (nu[i].nb != (nbr / 100))
		i++;
	ft_putstr(nu[i].st);
	write(1, " ", 1);
	i = 0;
	while (nu[i].nb != 100)
		i++;
	ft_putstr(nu[i].st);
	if (nbr % 100 != 0)
		write(1, " ", 1);
}
