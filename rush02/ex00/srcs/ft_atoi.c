/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 10:44:27 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 14:34:24 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parser.h"

long long unsigned int		ft_atoi(char *str)
{
	long long unsigned int	numb;
	int						i;

	i = 0;
	numb = 0;
	while (str[i])
	{
		numb = numb + (str[i] - '0');
		if (str[i + 1] && str[i + 1] >= '0' && str[i + 1] <= '9')
			numb = numb * 10;
		i++;
	}
	return (numb);
}
