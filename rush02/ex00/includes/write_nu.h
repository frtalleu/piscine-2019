/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_nu.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 14:41:50 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 16:44:33 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WRITE_NU_H
# define WRITE_NU_H

void						ft_write_tenth_unit(struct s_read_num *nu, int nbr);
void						ft_write_nu(struct s_read_num *nu,
		long long unsigned int nbr);
void						ft_write_big_nu(struct s_read_num *nu,
		long long unsigned int nb);
void						ft_check_write_nu(struct s_read_num *nu, char *str);
void						ft_putchar(char c);
void						ft_putstr(char *str);
long long unsigned int		ft_atoii(char *str);
void						ft_write_hundred
	(struct s_read_num *nu, long long unsigned int nbr);
int							ft_str_is_numeric(char *str);

#endif
