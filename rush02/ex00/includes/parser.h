/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 10:55:22 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 15:06:19 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PARSER_H
# define FT_PARSER_H
# include "struct.h"

long long unsigned int		ft_atoi(char *str);
t_strnum					*ft_parser(char *dict);

#endif
