/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vecremen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/17 20:22:37 by vecremen          #+#    #+#             */
/*   Updated: 2019/08/18 19:39:39 by vecremen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKDICT_H
# define CHECKDICT_H

int		ft_checkdict(char *dict);
int		ft_checkval(char (*tabnum)[32][12], int fd, char *dict);
int		ft_newval(int *fd, int *i, char *dict);
int		ft_checklines(char *dict);
int		ft_seqverif(int *fd, char (*buf)[2]);
void	ft_tab_to_20(char (*tabnum)[32][12]);
void	ft_tab_to_thousand(char (*tabnum)[32][12]);
void	ft_tab_to_billion(char (*tabnum)[32][12]);

#endif
