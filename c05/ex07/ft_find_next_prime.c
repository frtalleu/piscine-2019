/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/08 02:14:11 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/09 10:42:01 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_prime(int nb)
{
	int i;

	i = 2;
	if (nb <= 1)
		return (0);
	if (nb == 2)
		return (1);
	else
		while (i <= 46340)
		{
			if (nb % i == 0 && nb != i)
				return (0);
			i++;
		}
	return (1);
}

int		ft_find_next_prime(int nb)
{
	if (nb <= 2)
		return (2);
	if (nb % 2 == 0)
	{
		nb = nb + 1;
	}
	while (ft_is_prime(nb) != 1)
	{
		nb = nb + 2;
	}
	return (nb);
}
