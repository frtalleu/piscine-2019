/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/14 03:08:32 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/18 19:44:13 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		is_in_base(char c, char *base)
{
	int i;

	i = -1;
	while (base[++i])
		if (base[i] == c)
			return (i);
	return (-1);
}

int		word_count(char *str, char *charset)
{
	int n;
	int i;

	n = 0;
	i = 0;
	while (str[i])
	{
		if (is_in_base(str[i], charset) == -1)
		{
			while (str[i] != '\0' && is_in_base(str[i], charset) == -1)
				i++;
			n++;
			if (str[i] == '\0')
				return (n);
		}
		i++;
	}
	return (n);
}

int		word_length(char *start, char *charset)
{
	int i;

	i = 0;
	while (start[i] && is_in_base(start[i], charset) == -1)
		i++;
	return (i);
}

char	**init(int *word, int *i, char *s, char *cs)
{
	char **tab;

	*word = -1;
	*i = 0;
	if (!(tab = (char**)malloc(sizeof(char*) * (word_count(s, cs) + 1))))
		return (0);
	return (tab);
}

char	**ft_split(char *str, char *charset)
{
	int		i;
	int		word;
	int		len;
	char	**tab;
	int		j;

	tab = init(&word, &i, str, charset);
	while (++word < word_count(str, charset))
	{
		while (is_in_base(str[i], charset) != -1)
			i++;
		len = word_length(&str[i], charset);
		if (len >= 0)
		{
			tab[word] = (char*)malloc(sizeof(char) * (len + 1));
			j = -1;
			while (++j < len)
				tab[word][j] = str[i + j];
			tab[word][j] = '\0';
		}
		i += len;
	}
	tab[word_count(str, charset)] = 0;
	return (tab);
}
