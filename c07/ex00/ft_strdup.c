/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 12:10:14 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/19 23:43:56 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (0);
}

char	*ft_strdup(char *src)
{
	int		i;
	char	*c;

	c = NULL;
	i = 0;
	if (src == NULL)
		return (0);
	if (!(c = (char *)malloc(ft_strlen(src) * sizeof(char) + 1)))
		return (NULL);
	while (src[i] != '\0')
	{
		c[i] = src[i];
		i++;
	}
	c[i] = '\0';
	return (c);
}
