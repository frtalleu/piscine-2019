/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/12 23:49:06 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/19 23:51:34 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_malloc_size(int t, int a);
void	ft_putnbr_base(char *str, int nbr, char *base, int digit);

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int		is_in_base(char c, char *base)
{
	int i;

	i = 0;
	while (base[i] != '\0')
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (-1);
}

int		ft_capote(char *base)
{
	int i;
	int j;

	j = 0;
	i = 0;
	if (ft_strlen(base) == 0 || ft_strlen(base) == 1)
		return (0);
	while (base[i] != '\0')
	{
		while (base[j] != '\0')
		{
			if ((base[i] == base[j] && i != j) ||
					(base[j] == '+' || base[j] == '-' || base[j] == '\f' ||
					base[j] == '\t' || base[j] == ' ' || base[j] == '\n' ||
					base[j] == '\r' || base[j] == '\v'))
				return (0);
			j++;
		}
		j = 0;
		i++;
	}
	return (1);
}

int		ft_atoi_base(char *str, char *base)
{
	int i;
	int signe;
	int res;

	res = 0;
	signe = 1;
	i = 0;
	while (str[i] == '\f' || str[i] == '\t' || str[i] == ' '
			|| str[i] == '\n' || str[i] == '\r' || str[i] == '\v')
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			signe = signe * (-1);
		i++;
	}
	while (str[i] != '\0' && is_in_base(str[i], base) != (-1))
	{
		res = (res * ft_strlen(base)) + is_in_base(str[i], base);
		i++;
	}
	return (res * signe);
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int		d;
	int		i;
	int		e;
	char	*str;

	if (ft_capote(base_from) == 0 || ft_capote(base_to) == 0)
		return (NULL);
	i = ft_atoi_base(nbr, base_from);
	e = ft_strlen(base_to);
	d = ft_malloc_size(e, i);
	if (i == 0)
	{
		str = malloc(sizeof(char) * 2);
		str[0] = base_to[0];
		return (str);
	}
	if (i < 0)
		d++;
	str = malloc(sizeof(char) * (d));
	ft_putnbr_base(str, i, base_to, d - 1);
	str[d] = '\0';
	return (str);
}
