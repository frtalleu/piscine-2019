/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/13 02:32:28 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/17 21:34:28 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);

int		ft_malloc_size(int t, int a)
{
	unsigned int	nb;
	int				i;
	long int		j;

	j = 1;
	i = 0;
	if (a < 0)
		nb = a * (-1);
	else
		nb = a;
	while (j <= nb)
	{
		j = t * j;
		i++;
	}
	return (i);
}

void	ft_putnbr_base(char *str, int nbr, char *base, int digit)
{
	int				i;
	unsigned int	nb;
	int				j;

	j = ft_strlen(base);
	i = 0;
	if (nbr < 0)
	{
		str[0] = '-';
		nb = nbr * (-1);
		i++;
	}
	else
		nb = nbr;
	while (i <= digit)
	{
		str[digit] = base[nb % j];
		nb = nb / j;
		digit--;
	}
}
