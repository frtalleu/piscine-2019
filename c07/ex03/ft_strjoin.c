/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 16:28:56 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/19 23:45:18 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(int size, char **str, char *ctrop)
{
	int i;
	int j;
	int tmp;

	i = 0;
	j = 0;
	tmp = 0;
	if (size <= 0)
		return (1);
	while (i < size)
	{
		while (str[i][j])
			j++;
		tmp = tmp + j;
		i++;
		j = 0;
	}
	i = 0;
	while (ctrop[i])
		i++;
	i = i * (size - 1);
	tmp = tmp + i;
	return (tmp + 1);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	int		n;
	int		r;
	int		v;
	char	*y;

	n = -1;
	r = -1;
	v = -1;
	if (!(y = (char*)malloc(ft_strlen(size, strs, sep) * (sizeof(char)))))
		return (0);
	y[0] = size <= 0 ? '\0' : y[0];
	while (++n < size)
	{
		while (strs[n][++r])
			y[++v] = strs[n][r];
		r = -1;
		if (n < size - 1)
			while (sep[++r])
				y[++v] = sep[r];
		r = -1;
	}
	y[v + 1] = '\0';
	return (y);
}
