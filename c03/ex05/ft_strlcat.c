/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/04 04:06:51 by frtalleu          #+#    #+#             */
/*   Updated: 2019/08/05 09:06:33 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int tailledest;
	unsigned int j;
	unsigned int i;

	i = 0;
	while (dest[i] != '\0')
		i++;
	tailledest = i;
	j = 0;
	while (src[j] != '\0' && j < size - tailledest && tailledest < size)
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = '\0';
	while (src[j] != '\0')
		j++;
	if (tailledest < size)
		return (tailledest + j);
	return (size + j);
}
